"use strict";
exports.__esModule = true;
var NearestWhite = (function () {
    function NearestWhite(buffer) {
        this.buffer = buffer;
        this.parseInput();
        this.processCases();
        this.printNearestWhite();
    }
    NearestWhite.prototype.parseInput = function () {
        var convertedBuffer = this.buffer.toString().trim();
        if (!convertedBuffer.match(/^[\d\s]+$/))
            throw "Only numbers allowed";
        var input = convertedBuffer.split(" ");
        if (convertedBuffer.indexOf("\n") !== -1) {
            input = this.cleanupInput(input);
        }
        this.input = input;
    };
    NearestWhite.prototype.cleanupInput = function (input) {
        var formattedInput = input.map(function (i) {
            return i.split("\n");
        });
        return [].concat
            .apply([], formattedInput)
            .filter(function (el) { return el && el != ""; });
    };
    NearestWhite.prototype.processCases = function () {
        var casesInput = this.input.slice(1);
        this.cases = this.parseCases(casesInput, []);
    };
    NearestWhite.prototype.printNearestWhite = function () {
        var _this = this;
        this.cases.forEach(function (c) {
            process.stdout.write(_this.testCase(c));
        });
    };
    NearestWhite.prototype.parseCases = function (input, cases) {
        var casesInput = input;
        var caseLength = Number(casesInput[0]), caseShift = Number(caseLength) + 2, bitmap = casesInput.slice(2, caseShift);
        cases.push({
            rows: caseLength,
            columns: Number(casesInput[1]),
            bitmap: bitmap,
            whitePixels: this.getWhitePixelsIndexes(bitmap, 20)
        });
        casesInput = casesInput.slice(caseShift);
        return casesInput.length > 0 ? this.parseCases(casesInput, cases) : cases;
    };
    NearestWhite.prototype.findNearest = function (point, bitmap) {
        var found = [];
        var counter = 0;
        var x = point[0], y = point[1];
        if (bitmap[x][y] === "1") {
            return 0;
        }
        var x2, y2;
        x2 = x;
        y2 = y;
        var maxLen = bitmap[x].length > bitmap[x][0].length
            ? bitmap[x].length
            : bitmap[x][0].length;
        while (counter <= maxLen) {
            x2 = bitmap[x2 - counter] ? x2 - counter : x2;
            y2 = bitmap[x2] && bitmap[x2][y2 - counter] ? y2 - counter : y2;
            while (x2 < x + counter) {
                if (x2 &&
                    y2 &&
                    bitmap[x2] &&
                    bitmap[x2][y2] &&
                    bitmap[x2][y2] === "1") {
                    found.push(this.getDistance(point, [x2, y2]));
                }
                while (y2 < y + counter) {
                    if (bitmap[x2] && bitmap[x2][y2] && bitmap[x2][y2] === "1")
                        found.push(this.getDistance(point, [x2, y2]));
                    y2++;
                }
                while (y2 > y - counter) {
                    if (bitmap[x2] && bitmap[x2][y2] === "1")
                        found.push(this.getDistance(point, [x2, y2]));
                    y2--;
                }
                x2++;
            }
            while (x2 > x - counter) {
                if (bitmap[x2] && bitmap[x2][y2] === "1")
                    found.push(this.getDistance(point, [x2, y2]));
                while (y2 > y - counter) {
                    if (bitmap[x2] && bitmap[x2][y2] === "1")
                        found.push(this.getDistance(point, [x2, y2]));
                    y2--;
                }
                while (y2 < y + counter) {
                    if (bitmap[x2] && bitmap[x2][y2] && bitmap[x2][y2] === "1")
                        found.push(this.getDistance(point, [x2, y2]));
                    y2++;
                }
                x2--;
            }
            counter++;
        }
        return found.reduce(function (p, v) {
            return p < v ? p : v;
        });
    };
    NearestWhite.prototype.testCase = function (_a) {
        var bitmap = _a.bitmap, columns = _a.columns, rows = _a.rows, whitePixels = _a.whitePixels;
        try {
            var result = [];
            for (var i = 0; i < rows; i++) {
                for (var j = 0; j < columns; j++) {
                    var distance = this.findNearestWhitePixel([i, j], bitmap, whitePixels);
                    result.push(distance + " ");
                }
                result.push("\n");
            }
            result.push("\n");
            return result.join("");
        }
        catch (error) {
            console.log("Error while running the test cases", error);
            process.exit(1);
        }
    };
    NearestWhite.prototype.findNearestWhitePixel = function (point, bitmap, whitePixels) {
        try {
            if (!point) {
                throw "Input malformed";
            }
            var x = point[0], y = point[1];
            if (!bitmap[x][y]) {
                throw "Wrong Input. Value at point [" + x + "," + y + "] not found " + bitmap[x];
            }
            if (bitmap[x][y] == "1") {
                return 0;
            }
            return this.getDistances(point, whitePixels, [], 0);
        }
        catch (error) {
            console.log("Error while searching nearestWhitePixel: ", error);
            process.exit(1);
        }
    };
    NearestWhite.prototype.getDistances = function (point, whitePixels, result, start) {
        if (!whitePixels[start]) {
            return result.reduce(function (p, v) {
                return p < v ? p : v;
            });
        }
        var whites = whitePixels[start];
        for (var i = 0, l = whites.length; i < l; i++) {
            var distance = this.getDistance(point, [start, whites[i]]);
            result.push(distance);
        }
        return this.getDistances(point, whitePixels, result, start + 1);
    };
    NearestWhite.prototype.getDistance = function (p1, p2) {
        var distance = Math.abs(p2[0] - p1[0]) + Math.abs(p2[1] - p1[1]);
        return distance;
    };
    NearestWhite.prototype.getWhitePixelsIndexes = function (bitmap, rows) {
        var indexes = [];
        for (var i = 0; i < rows; i++) {
            if (!bitmap[i])
                break;
            indexes[i] = this.getWhiteIndices(bitmap[i].toString()).slice();
        }
        return indexes;
    };
    NearestWhite.prototype.getWhiteIndices = function (string) {
        var result = [];
        var match;
        var regex = new RegExp(/1/g);
        while ((match = regex.exec(string)))
            result.push(match.index);
        return result;
    };
    return NearestWhite;
}());
exports["default"] = NearestWhite;
//# sourceMappingURL=NearestWhite.js.map