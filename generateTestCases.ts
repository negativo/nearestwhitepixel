main();

function main() {
  const maxCases: number = 2;
  const maxRows: number = 182;
  const totalCases: number = getRandom(maxCases);
  let casesOutput: string = totalCases + " \n";

  for (let i = 0; i < totalCases; i++) {
    const rows = getRandom(maxRows);
    const cols = getRandom(maxRows);
    casesOutput += rows.toString() + " " + cols.toString() + " \n";

    for (let j = 0; j < rows; j++) {
      for (let k = 0; k < cols; k++) {
        let val = Math.round(Math.random());
        casesOutput.concat(val.toString());
      }

      casesOutput += " \n";
    }
    casesOutput += " \n";
  }
  casesOutput = casesOutput.substring(0, casesOutput.length - 2);
  process.stdout.write(casesOutput);
}

function getRandom(max: number) {
  return Math.floor(Math.random() * max) + 1;
}
