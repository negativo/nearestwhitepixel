type TestCase = {
  bitmap: string[];
  rows: number;
  columns: number;
  whitePixels: number[][];
};
type Point = number[];
export default class NearestWhite {
  buffer: string[];
  input: string[];
  cases: TestCase[];

  constructor(buffer: string[]) {
    this.buffer = buffer;
    this.parseInput();
    this.processCases();
    this.printNearestWhite();
  }

  parseInput() {
    let convertedBuffer: string = this.buffer.toString().trim();
    if (!convertedBuffer.match(/^[\d\s]+$/)) throw "Only numbers allowed";
    let input: string[] = convertedBuffer.split(" ");

    if (convertedBuffer.indexOf("\n") !== -1) {
      input = this.cleanupInput(input);
    }
    this.input = input;
  }

  cleanupInput(input: string[]): string[] {
    const formattedInput = input.map(i => {
      return i.split("\n");
    });
    return [].concat
      .apply([], formattedInput)
      .filter((el: string) => el && el != "");
  }
  processCases() {
    const casesInput: string[] = this.input.slice(1);
    this.cases = this.parseCases(casesInput, []);
  }

  printNearestWhite() {
    this.cases.forEach(c => {
      process.stdout.write(this.testCase(c));
    });
  }
  parseCases(input: string[], cases: TestCase[]): TestCase[] {
    let casesInput: string[] = input;

    const caseLength: number = Number(casesInput[0]),
      caseShift: number = Number(caseLength) + 2,
      bitmap: string[] = casesInput.slice(2, caseShift);

    cases.push({
      rows: caseLength,
      columns: Number(casesInput[1]),
      bitmap: bitmap,
      whitePixels: this.getWhitePixelsIndexes(bitmap, 20)
    });
    casesInput = casesInput.slice(caseShift);

    return casesInput.length > 0 ? this.parseCases(casesInput, cases) : cases;
  }

  /*
   * Different approach, start searching around the pixel and expand the search.
   * Althought the result for small datasets is correct, computation is taking too long for bigger datasets.
   */

  findNearest(point: Point, bitmap: string[]) {
    let found = [];
    let counter = 0;
    const x = point[0],
      y = point[1];
    if (bitmap[x][y] === "1") {
      return 0;
    }
    let x2, y2;
    x2 = x;
    y2 = y;
    const maxLen =
      bitmap[x].length > bitmap[x][0].length
        ? bitmap[x].length
        : bitmap[x][0].length;
    while (counter <= maxLen) {
      x2 = bitmap[x2 - counter] ? x2 - counter : x2;
      y2 = bitmap[x2] && bitmap[x2][y2 - counter] ? y2 - counter : y2;

      while (x2 < x + counter) {
        if (
          x2 &&
          y2 &&
          bitmap[x2] &&
          bitmap[x2][y2] &&
          bitmap[x2][y2] === "1"
        ) {
          found.push(this.getDistance(point, [x2, y2]));
        }
        while (y2 < y + counter) {
          if (bitmap[x2] && bitmap[x2][y2] && bitmap[x2][y2] === "1")
            found.push(this.getDistance(point, [x2, y2]));
          y2++;
        }
        while (y2 > y - counter) {
          if (bitmap[x2] && bitmap[x2][y2] === "1")
            found.push(this.getDistance(point, [x2, y2]));
          y2--;
        }
        x2++;
      }

      while (x2 > x - counter) {
        if (bitmap[x2] && bitmap[x2][y2] === "1")
          found.push(this.getDistance(point, [x2, y2]));

        while (y2 > y - counter) {
          if (bitmap[x2] && bitmap[x2][y2] === "1")
            found.push(this.getDistance(point, [x2, y2]));
          y2--;
        }
        while (y2 < y + counter) {
          if (bitmap[x2] && bitmap[x2][y2] && bitmap[x2][y2] === "1")
            found.push(this.getDistance(point, [x2, y2]));
          y2++;
        }
        x2--;
      }

      counter++;
    }
    return found.reduce(function(p, v) {
      return p < v ? p : v;
    });
  }
  testCase({ bitmap, columns, rows, whitePixels }: TestCase): string {
    try {
      const result = [];

      for (let i = 0; i < rows; i++) {
        for (let j = 0; j < columns; j++) {
          let distance = this.findNearestWhitePixel(
            [i, j],
            bitmap,
            whitePixels
          );
          //let distance = this.findNearest([i, j], bitmap);
          result.push(distance + " ");
        }
        result.push("\n");
      }
      result.push("\n");
      return result.join("");
    } catch (error) {
      console.log("Error while running the test cases", error);
      process.exit(1);
    }
  }

  findNearestWhitePixel(
    point: Point,
    bitmap: string[],
    whitePixels: number[][]
  ): number {
    try {
      if (!point) {
        throw "Input malformed";
      }
      const x = point[0],
        y = point[1];

      if (!bitmap[x][y]) {
        throw `Wrong Input. Value at point [${x},${y}] not found ${bitmap[x]}`;
      }

      if (bitmap[x][y] == "1") {
        return 0;
      }

      return this.getDistances(point, whitePixels, [], 0);
    } catch (error) {
      console.log("Error while searching nearestWhitePixel: ", error);
      process.exit(1);
    }
  }

  getDistances(
    point: Point,
    whitePixels: number[][],
    result: number[],
    start: number
  ): number {
    if (!whitePixels[start]) {
      return result.reduce(function(p, v) {
        return p < v ? p : v;
      });
    }
    let whites = whitePixels[start];
    for (let i = 0, l = whites.length; i < l; i++) {
      let distance = this.getDistance(point, [start, whites[i]]);
      result.push(distance);
    }

    return this.getDistances(point, whitePixels, result, start + 1);
  }
  getDistance(p1: Point, p2: Point): number {
    const distance = Math.abs(p2[0] - p1[0]) + Math.abs(p2[1] - p1[1]);
    return distance;
  }
  getWhitePixelsIndexes(bitmap: string[], rows: number) {
    const indexes: number[][] = [];
    for (let i = 0; i < rows; i++) {
      if (!bitmap[i]) break;
      indexes[i] = [...this.getWhiteIndices(bitmap[i].toString())];
    }
    return indexes;
  }

  getWhiteIndices(string: string): number[] {
    let result: number[] = [];
    let match;
    const regex = new RegExp(/1/g);
    while ((match = regex.exec(string))) result.push(match.index);

    return result;
  }
}
