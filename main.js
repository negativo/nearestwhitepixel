"use strict";
exports.__esModule = true;
var NearestWhite_1 = require("./NearestWhite");
var stdin = process.openStdin();
stdin.addListener("data", function (buffer) {
    try {
        new NearestWhite_1["default"](buffer);
    }
    catch (error) {
        console.log("Error processing the input:", error);
        process.exit(1);
    }
});
//# sourceMappingURL=main.js.map