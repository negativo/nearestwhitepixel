main();
function main() {
    var maxCases = 2;
    var maxRows = 182;
    var totalCases = getRandom(maxCases);
    var casesOutput = totalCases + " \n";
    for (var i = 0; i < totalCases; i++) {
        var rows = getRandom(maxRows);
        var cols = getRandom(maxRows);
        casesOutput += rows.toString() + " " + cols.toString() + " \n";
        for (var j = 0; j < rows; j++) {
            for (var k = 0; k < cols; k++) {
                var val = Math.round(Math.random());
                casesOutput.concat(val.toString());
            }
            casesOutput += " \n";
        }
        casesOutput += " \n";
    }
    casesOutput = casesOutput.substring(0, casesOutput.length - 2);
    process.stdout.write(casesOutput);
}
function getRandom(max) {
    return Math.floor(Math.random() * max) + 1;
}
//# sourceMappingURL=generateTestCases.js.map