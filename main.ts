import NearestWhite from "./NearestWhite";

const stdin = process.openStdin();

stdin.addListener("data", function(buffer) {
  try {
    new NearestWhite(buffer);
  } catch (error) {
    console.log("Error processing the input:", error);
    process.exit(1);
  }
});
